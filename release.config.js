module.exports = {
  branches: ['master'],
  plugins: [
    [
      '@semantic-release/commit-analyzer'
    ],
    [
      '@semantic-release/release-notes-generator',
      {
        preset: 'conventionalcommits',
        presetConfig: {
          types: [
            { type: 'feat', section: 'Новые функции' },
            { type: 'fix', section: 'Исправления' },
            { type: 'style', section: 'Стилистические исправления' },
          ],
        },
      },
    ],
    [
      '@semantic-release/changelog',
    ],
    ['@semantic-release/npm'],
    [
      '@semantic-release/git',
      {
        assets: [
          'CHANGELOG.md',
          'package.json',
          'package-lock.json',
        ],
        "message": "chore(release) ${nextRelease.version} skip"
      },
    ]
  ],
};
