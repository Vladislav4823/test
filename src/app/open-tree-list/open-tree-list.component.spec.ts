import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenTreeListComponent } from './open-tree-list.component';

describe('OpenTreeListComponent', () => {
  let component: OpenTreeListComponent;
  let fixture: ComponentFixture<OpenTreeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenTreeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenTreeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
