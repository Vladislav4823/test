import {Component, Input, OnInit} from '@angular/core';
import {Office} from '../office';
import {Branch} from '../branch';
import {LinkTreeComponent} from '../link-tree/link-tree.component';

@Component({
  selector: 'app-open-tree-list',
  templateUrl: './open-tree-list.component.html',
  styleUrls: ['./open-tree-list.component.css']
})
export class OpenTreeListComponent implements OnInit {
@Input() office: Office[];
  filial: Branch[] = [];
  toggle = false;
  constructor(private linkTreeComponent: LinkTreeComponent) { }



  getBranchID(id: number, e): void {
    this.filial.splice(0, this.filial.length);
    for (this.linkTreeComponent.selectedBranch of this.linkTreeComponent.branches) {
      if (this.linkTreeComponent.selectedBranch.parentsID === id) {
        this.filial.push(this.linkTreeComponent.selectedBranch);
        this.toggle = true;
      }
    }
    for (let j in e.path) {
        if (e.path[j].id === "namediv") {
          if (e.path[j].children[1].children.length != 0){
            this.filial.splice(0, this.filial.length);
            this.toggle = false;
          }
      }
    }
  }

  ngOnInit() {
    console.log(this.office);
  }

}
