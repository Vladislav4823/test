import {Component, EventEmitter, OnInit, Output, Input, Injectable} from '@angular/core';
import { AppComponent } from '../app.component';
import {User} from '../user';
import { UserService } from '../user.service';
import {Branch} from '../branch';
import {consoleTestResultHandler} from 'tslint/lib/test';

@Component({
  selector: 'app-authoriz',
  templateUrl: './authoriz.component.html',
  styleUrls: ['./authoriz.component.css']
})
export class AuthorizComponent implements OnInit {
  users: User[];
  balert = false;
  selectedUser: User;
  @Output() childEvent = new EventEmitter();
  testChild(name: string, pass: string) {
    name = name.trim();
    if (name || pass) {
      this.balert = false;
      for (this.selectedUser of this.users) {
        if (this.selectedUser.name === name.trim() && this.selectedUser.password === pass) {
          this.app.role = this.selectedUser.roles;
          this.app.name = this.selectedUser.name;
          this.app.test();
          this.balert = true;
          break;
        }
      }
  }
    if (this.balert === false) {
      alert('Проверьте данные');
    }
  }
  getUsers(): void {
    this.userServices.getUsers()
      .subscribe(users => this.users = users);
  }
  constructor(private userServices: UserService,
              private app: AppComponent) { }

  ngOnInit() {
    this.getUsers();
  }

}
