import { Component, Input } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  role: string;
  name: string;
  toggle = true;      // Change this for logins!
  title = 'Учёт филиальной сети предприятия';
  test() {
    this.toggle = ! this.toggle;
  }

}
