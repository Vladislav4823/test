export class Office {
  id: number;
  FullName: string;
  Name: string;
  INN: number;
  KPP: number;
  Founder: string;
  Address: string;
  Phone: number;
}
