import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListBranchComponent } from './list-branch/list-branch.component';
import {AuthorizComponent} from './authoriz/authoriz.component';
import {AppComponent} from './app.component';
import {InterfaceComponent} from './interface/interface.component';
import {LinkTreeComponent} from './link-tree/link-tree.component';

const routes: Routes = [
  { path: 'list', component: ListBranchComponent},
  { path: 'authorization', component: AuthorizComponent},
  { path: 'base', component: InterfaceComponent},
  { path: 'tree', component: LinkTreeComponent},
  {path: '', redirectTo: '/authorization', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
