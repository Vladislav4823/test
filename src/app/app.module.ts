import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthorizComponent } from './authoriz/authoriz.component';
import { ListBranchComponent } from './list-branch/list-branch.component';
import {FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';
import { InterfaceComponent } from './interface/interface.component';
import { LinkTreeComponent } from './link-tree/link-tree.component';
import { OpenTreeListComponent } from './open-tree-list/open-tree-list.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthorizComponent,
    ListBranchComponent,
    InterfaceComponent,
    LinkTreeComponent,
    OpenTreeListComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, {dataEncapsulation: false}
    ),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
