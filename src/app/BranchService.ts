import { Injectable } from '@angular/core';
import { Branch } from './branch';
import { Observable, of } from 'rxjs';
import {catchError, tap, map} from 'rxjs/operators';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {Office} from './office';

@Injectable({
  providedIn: 'root'
})
export class BranchService {
  private branchesUrl = 'api/branches';
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };
  getBranches(): Observable<Branch[]> {
    return this.http.get<Branch[]>(this.branchesUrl);
  }
  updateBranch(branch: Branch): Observable<any> {
    return this.http.put(this.branchesUrl, branch, this.httpOptions);
  }
  addBranch(bracnh: Branch): Observable<Branch> {
    return this.http.post<Branch>(this.branchesUrl, bracnh, this.httpOptions);
  }
  constructor(private http: HttpClient) { }
}
