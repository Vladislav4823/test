import { Injectable } from '@angular/core';
import { Office } from './office';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {Branch} from './branch';

@Injectable({
  providedIn: 'root'
})
export class OfficeService {
  private officeUrl = 'api/offices';
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };
  getOffices(): Observable<Office[]> {
    return this.http.get<Office[]>(this.officeUrl);
  }
  updateOffice(office: Office): Observable<any> {
    return this.http.put(this.officeUrl, office, this.httpOptions);
  }
  addOffices(office: Office): Observable<Office> {
    return this.http.post<Office>(this.officeUrl, office, this.httpOptions);
  }
  constructor(private http: HttpClient) { }
}
