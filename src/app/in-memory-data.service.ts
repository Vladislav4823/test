import { Injectable } from '@angular/core';
import { InMemoryDbService} from 'angular-in-memory-web-api';
import { User } from './user';
import { Branch } from './branch';
import {Office} from './office';


@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const branches = [
      {id: 1, address: 'krasrab 2', phone: 2357944, name: 'Пупкин Ivan Fedorovich', parentsID: 2},
      {id: 3, address: 'krasrab 2', phone: 2357944, name: 'Pupkin Ivan Fedorovich', parentsID: 2},
      {id: 4, address: 'krasrab 2', phone: 2357944, name: 'Ивашкин Ivan Fedorovich', parentsID: 2},
      {id: 2, address: 'krasrab 22', phone: 23257944, name: 'Ivanov Ivan Alex', parentsID: 1}
    ];
    const users = [
      {id: 1, name: 'admin', password: 'admin', roles: 'администратор'},
      {id: 2, name: 'tryxa', password: '123', roles: 'сотрудник'},
      {id: 3, name: 'tryx22a', password: '1523', roles: 'сотрудник'}
    ];
    const offices = [
      { id: 1, FullName: 'Full Name Org', Name: 'OOO zum', INN: 12565,
        KPP: 5155, Founder: 'Petrov petr', Address: 'lenina 154', Phone: 126954 },
      { id: 2, FullName: 'Full Name Organiz', Name: 'OAO zummer', INN: 12535, KPP: 1115,
        Founder: 'BOSS', Address: 'lenina 14', Phone: 1254 }
    ];
    return {branches, offices, users};
}
  // genIId(users: User[]): number {
  //   return users.length > 0 ? Math.max(...users.map(user => user.id)) + 1 : 1;
  // }
  genId<T extends User | Branch | Office>(myTable: T[]): number {
    return myTable.length > 0 ? Math.max(...myTable.map(t => t.id)) + 1 : 1;
  }
  constructor() { }
}
