import { Component, OnInit } from '@angular/core';
import {Branch} from '../branch';
import {Office} from '../office';
import {BranchService} from '../BranchService';
import {OfficeService} from '../OfficeService';
import {Location} from '@angular/common';

@Component({
  selector: 'app-link-tree',
  templateUrl: './link-tree.component.html',
  styleUrls: ['./link-tree.component.css']
})
export class LinkTreeComponent implements OnInit {
  branches: Branch[];
  selectedBranch: Branch;
  offices: Office[];
  selectedOffice: Office;
  constructor(private branchService: BranchService,
              private officeService: OfficeService,
              private location: Location) { }

  ngOnInit() {
    this.getBranches();
    this.getOffices();
  }
  getBranches(): void {
    this.branchService.getBranches()
      .subscribe(branches => this.branches = branches);
  }
  getOffices(): void {
    this.getBranches();
    this.officeService.getOffices()
      .subscribe(offices => this.offices = offices);
  }
  onSelectBranch(branch: Branch): void {
    console.log(branch)
    this.selectedBranch = branch;
  }
  onSelectOffice(office: Office): void {
    console.log(office)
    this.selectedOffice = office;
  }
  goBack(): void {
    this.location.back();
  }
  save(): void {
    this.branchService.updateBranch(this.selectedBranch)
      .subscribe(() => this.goBack());
  }



    // this.branchService.getBranches()
    //   .subscribe(branches => this.branches = branches);

}
