import { Component, OnInit } from '@angular/core';
import { BranchService} from '../BranchService';
import { OfficeService} from '../OfficeService';
import {getNumberOfCurrencyDigits, Location} from '@angular/common';
import {Branch} from '../branch';
import { Office } from '../office';
import {toNumbers} from '@angular/compiler-cli/src/diagnostics/typescript_version';
import {AuthorizComponent} from '../authoriz/authoriz.component';
import {AppComponent} from '../app.component';


@Component({
  selector: 'app-list-branch',
  templateUrl: './list-branch.component.html',
  styleUrls: ['./list-branch.component.css']
})
export class ListBranchComponent implements OnInit {
  addOffice = false;
  addBranch = false;
  stroke = 'Добавить филиал';
  strooke = 'Добавить головной офис';
  branches: Branch[];
  selectedBranch: Branch;
  offices: Office[];
  selectedOffice: Office;
  constructor(private branchService: BranchService,
              private officeService: OfficeService,
              private location: Location,
              private app: AppComponent) { }

  ngOnInit() {

    this.getBranches();
    this.getOffices();
  }
  getBranches(): void {
    this.branchService.getBranches()
      .subscribe(branches => this.branches = branches);
  }
  getOffices(): void {
    this.officeService.getOffices()
      .subscribe(offices => this.offices = offices);
  }
  onSelectBranch(branch: Branch): void {
    if (this.app.role === 'администратор') {
      this.selectedBranch = branch;
    }
  }
  onSelectOffice(office: Office): void {
    if (this.app.role === 'администратор') {
      this.selectedOffice = office;
    }
  }

  save(): void {
    this.branchService.updateBranch(this.selectedBranch)
      .subscribe(this.selectedBranch = null);
  }
  saveOf(): void {
    this.officeService.updateOffice(this.selectedOffice)
      .subscribe(this.selectedOffice = null);
}
  addBr(): void{
    if (this.addBranch)
      this.stroke = 'Добавить филиал';
    else this.stroke = 'Скрыть';
    this.addBranch = ! this.addBranch;
  }
  addOf(): void{
    if (this.addOffice) this.strooke = 'Добавить головной офис';
    else this.strooke = 'Скрыть';
    this.addOffice = ! this.addOffice;
  }
  addOff(FullName, Name, INN, KPP, Founder, Address, Phone): void {
    if (!Address || !Founder || !KPP || !FullName || !Name || !Phone || !INN) { return; }
    Address = Address.trim();
    Founder = Founder.trim();
    FullName = FullName.trim();
    Name = Name.trim();
    this.officeService.addOffices({FullName, Name, INN, KPP, Founder, Address, Phone} as Office)
      .subscribe(office => {this.offices.push(office); this.addOf();});
  }
  addBrr(name, address, phone, iid): void {
    let parentsID;
    address = address.trim();
    name = name.trim();
    parentsID = Number(iid);
    if (!address || !name || !phone || !parentsID || parentsID > this.offices.length) { return; }
    this.branchService.addBranch({address, phone, name, parentsID} as Branch)
      .subscribe(branch => {this.branches.push(branch); this.addBr();});
  }

}
