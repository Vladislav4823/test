export class Branch {
  id: number;
  address: string;
  phone: number;
  name: string;
  parentsID: number;
}
