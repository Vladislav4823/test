## [1.2.0](https://gitlab.com/Vladislav4823/test/compare/v1.1.0...v1.2.0) (2024-01-23)


### Новые функции

* **semantic:** try clear changelog ([12d49e7](https://gitlab.com/Vladislav4823/test/commit/12d49e7b6ee6f40d8c6d88767395331e5ffb68f8))

## [1.1.0](https://gitlab.com/Vladislav4823/test/compare/v1.0.12...v1.1.0) (2024-01-23)


### Новые функции

* ss ([7745086](https://gitlab.com/Vladislav4823/test/commit/7745086890da344b188b8a836ef5f24637bcf51b))
* ss1 ([73d6200](https://gitlab.com/Vladislav4823/test/commit/73d620042e2199d4bfde8c4f44973d9574e8e167))


### Исправления

* **ss:** skip ci ([f59b8d2](https://gitlab.com/Vladislav4823/test/commit/f59b8d2ff5e62af51ba3e20f860e096aa474df18))

## [1.0.12](https://gitlab.com/Vladislav4823/test/compare/v1.0.11...v1.0.12) (2024-01-19)


### Исправления

* **ss:** skip ci ([508dc65](https://gitlab.com/Vladislav4823/test/commit/508dc65806e492da5d36f3e7156d508541327388))

## [1.0.11](https://gitlab.com/Vladislav4823/test/compare/v1.0.10...v1.0.11) (2024-01-18)


### Исправления

* **ss:** skip ci ([34820bc](https://gitlab.com/Vladislav4823/test/commit/34820bc8560fdc0507ece67f244bbe427c547964))

## [1.0.10](https://gitlab.com/Vladislav4823/test/compare/v1.0.9...v1.0.10) (2024-01-18)


### Исправления

* **ss:** skip ci ([d1143be](https://gitlab.com/Vladislav4823/test/commit/d1143bed8addf659cc036568d86961bc19154186))

## [1.0.9](https://gitlab.com/Vladislav4823/test/compare/v1.0.8...v1.0.9) (2024-01-18)


### Исправления

* **ss:** skip ci ([8ade4de](https://gitlab.com/Vladislav4823/test/commit/8ade4de02ae2324cbd184d9f06066549d268e87d))
* **ss:** skip ci ([8b2ce4c](https://gitlab.com/Vladislav4823/test/commit/8b2ce4c7e81b61c5bff8c3c0a174bae767374299))
* **ss:** skip ci ([78fd2b7](https://gitlab.com/Vladislav4823/test/commit/78fd2b71ab3e0d9d0b29ddb8600c9b0f0f1e4751))

## [1.0.8](https://gitlab.com/Vladislav4823/test/compare/v1.0.7...v1.0.8) (2024-01-17)


### Исправления

* **ss:** skip ci ([729785d](https://gitlab.com/Vladislav4823/test/commit/729785df59b3a92fc7f9b90b7f1b3d26d037b9d1))
* **ss:** skip ci ([f09d887](https://gitlab.com/Vladislav4823/test/commit/f09d887612be5fb894d91a8fb66fa315f0094fbf))

## [1.0.7](https://gitlab.com/Vladislav4823/test/compare/v1.0.6...v1.0.7) (2024-01-17)


### Исправления

* **ss:** skip ci ([7e6d4bd](https://gitlab.com/Vladislav4823/test/commit/7e6d4bd3a23e4a5221c2eb363c7ecd4d7f3a9a89))

## [1.0.6](https://gitlab.com/Vladislav4823/test/compare/v1.0.5...v1.0.6) (2024-01-17)


### Исправления

* **ss:** skip ci ([0a1ba93](https://gitlab.com/Vladislav4823/test/commit/0a1ba936db1650e682936155c02db42f35f3db1e))

## [1.0.5](https://gitlab.com/Vladislav4823/test/compare/v1.0.4...v1.0.5) (2024-01-17)


### Исправления

* **ss:** skip ci ([0f33517](https://gitlab.com/Vladislav4823/test/commit/0f33517e575e6615bed9c8db89a929e0c07c5f5c))

## [1.0.4](https://gitlab.com/Vladislav4823/test/compare/v1.0.3...v1.0.4) (2024-01-17)


### Исправления

* **ss:** skip ci ([e1e19e2](https://gitlab.com/Vladislav4823/test/commit/e1e19e2de494791a45254623f952a709ec4cdb27))

## [1.0.3](https://gitlab.com/Vladislav4823/test/compare/v1.0.2...v1.0.3) (2024-01-17)


### Исправления

* **ss:** skip ci ([2d2e3af](https://gitlab.com/Vladislav4823/test/commit/2d2e3af04de4100e53a28774eeff06bba3bbe581))

## [1.0.2](https://gitlab.com/Vladislav4823/test/compare/v1.0.1...v1.0.2) (2024-01-17)


### Исправления

* **ss:** skip ci ([8256e26](https://gitlab.com/Vladislav4823/test/commit/8256e26b7bf280bf41a759b4e947cb73c6411939))
* **ss:** skip ci ([fa1c357](https://gitlab.com/Vladislav4823/test/commit/fa1c357e45f7d34c4a0c1bb40ec1806face79d09))
* **ss:** skip ci ([69abab3](https://gitlab.com/Vladislav4823/test/commit/69abab305e11edfce1b765900675d7381f938192))

## [1.0.1](https://gitlab.com/Vladislav4823/test/compare/v1.0.0...v1.0.1) (2024-01-17)


### Исправления

* **ss:** skip ci ([9a5b9f9](https://gitlab.com/Vladislav4823/test/commit/9a5b9f98499c9c5c2f63ffdab70f65b8c56284d1))

## 1.0.0 (2024-01-17)


### Новые функции

* 1.0.0 ([96c82ee](https://gitlab.com/Vladislav4823/test/commit/96c82ee14336e3021bb5137543e078b7130aa3d2))
* 1.1.0 ([4e4670e](https://gitlab.com/Vladislav4823/test/commit/4e4670e63ef1a2a06d3339652b060671799139de))
* **11:** 1.0.0 ([294eadb](https://gitlab.com/Vladislav4823/test/commit/294eadb8315d54bf3910d12a2c204217fd39e770))
* **app:** say hello 1.1.0 ([47d1680](https://gitlab.com/Vladislav4823/test/commit/47d1680ba83a507955d0991594ebd50b3262968b))
* asd ([f6a5b8f](https://gitlab.com/Vladislav4823/test/commit/f6a5b8f77ca529c805b34876bed096b6e65384a7))
* **gitlab:** remove first stage ([0780ca0](https://gitlab.com/Vladislav4823/test/commit/0780ca0641293d7a52ab566bea2e81722b3df9c8))
* initial commit ([b2664d6](https://gitlab.com/Vladislav4823/test/commit/b2664d65bed9da6c52e1f74f40882ed3fae2fc3a))
* push ([8ad830a](https://gitlab.com/Vladislav4823/test/commit/8ad830a92d1d9dd8d627e0948803dcd8623633af))
* push ([a14dbde](https://gitlab.com/Vladislav4823/test/commit/a14dbde66a01c8e48167042cdb88f750ec94aee2))
* sad ([933be4d](https://gitlab.com/Vladislav4823/test/commit/933be4dc0b26504b31c689263d74591715b7569a))
* sad ([e3124aa](https://gitlab.com/Vladislav4823/test/commit/e3124aa6fec95c1813260718fdf203dde946ffe9))
* test all comment ([45c8a4c](https://gitlab.com/Vladislav4823/test/commit/45c8a4cd83502b7a2db4558d3164070668f6aea0))
* without tag test ([bdb4352](https://gitlab.com/Vladislav4823/test/commit/bdb4352f092fe9b6c31b731e5b54301de7c415c7))
* without tag test notw work return ([5892342](https://gitlab.com/Vladislav4823/test/commit/5892342b9c71a26c64afb79b1f2dcfd491d7eaa5))


### Исправления

* **app:** remove hello  v 1 0 3 ([9f242c8](https://gitlab.com/Vladislav4823/test/commit/9f242c815b7d581ee0a85ad95a4af8a8ec6d34b5))
* **package:** v 1 0 3 ([b20c286](https://gitlab.com/Vladislav4823/test/commit/b20c286a96ebd19355a1b40d82ac756c90e7c68f))
